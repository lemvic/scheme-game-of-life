## Chez Scheme Game of Life

This is an implementation of the [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) in [Chez Scheme](https://github.com/cisco/ChezScheme).

It is intended as a learning excercise in Scheme programming and optimization.

### Running

Program is divided into a library [game-of-life](./game-of-life.ss) and [entry point](./run.ss). To run the program, type in the following:

```
scheme --libdirs <path-to-repo-folder> --program <path-to-run-randomized.ss> -- <field-width> <field-height> <iterations> <random-seed>
```
